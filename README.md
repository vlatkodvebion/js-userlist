# README #

First, change the title of the page to 'JS Users List'

You need to display a list of users. The list of users can be obtained in two ways, first to use the list of users that is available in the main.js file and the second is to make an http get request to the following endpoint 
[https://randomuser.me/api/?results=25&inc=name,dob,email,picture](https://randomuser.me/api/?results=25&inc=name,dob,email,picture)

For each of the users calculate the age and add a new property named 'age' with the calculated value. For the age calculation you can use pure javascript or you can use moment.js.
Display the list of users in the div with id = 'container'. For each of the users you should display full name, email, age and avatar. Add css styling to make the list look as much close to the list in the image:
 ![User List](./users.png)

You are allowed to use the following resources:

[https://developer.mozilla.org/en-US/](https://developer.mozilla.org/en-US/)

[https://momentjs.com/](https://momentjs.com/)